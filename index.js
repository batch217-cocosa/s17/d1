// Functions

function printName(){
    console.log("My name is John")
}


printName(); /*Invocation - callinf a function that need to be executed*/





function declaredFunction(){

    console.log("Hello again! from declaredFuntion");
}



// Function Expression
// A functrion can also be stored in a variable. This is called a function expression.


let variableFunction = function(){

console.log("Hello Again!");

}

variableFunction();

let funcExpression = function funcname(){
    console.log("Hello from the other side.");
}

funcExpression();


funcExpression = function(){
    console.log("updated funcExpression");
}

funcExpression();
funcExpression();
funcExpression();



// reassgning declaredFunction() value


declaredFunction = function(){
    console.log("Updated declaredFunction")
}

declaredFunction();


const constantFunc = function(){
    console.log("Initialized with const")
}


// Re-assignemnt of a const function



// constantFunc = function(){
//  console.log("Can we reassign it?")
// }



// Scope


// 1. local/block scope
// 2. Global scope 
// 3. Function scope




{

    let localVar = "Armando Perez";

    console.log(localVar);

}




let globalScope = "Mr. Worldwide";

console.log(globalScope);



// function Scoping

function showNames (){
    // Funtion scoped variables

    const functionConst = "John";
    let functionLet = "Jane";


    console.log(functionConst);
    console.log(functionLet);
}


showNames();


    // console.log(functionConst);
    // console.log(functionLet);
    // --> cannot be used because these variables are located insode the scope os a function


    // Nested function
    // You can create another function inside a function/ This is called a nested function. This nmesdted function, being inside the myNewFunction will have access to the variable, name, as they are within the same scope/code block.


    function myNewFunction(){
        let name = "Jane"; //inherited data

        function nestedFunction(){
            let nestedName = "John";
            console.log(name);
        }
        nestedFunction();
    }

    myNewFunction();


    // nestedFunction(); --> will cause error because the function is locate to a function scoped parent function

    // Function and Global Scoped Variable

    // Global scoped variable

    let globalName = "Alejandro";

    function myNewFunction2(){
        let nameInside = "Renz";



        // Variables declared Globally (outside any function) have global scope
        // global variables can accessed from anywhere in a javascript
        // program including from inside afunction

        console.log(nameInside);
    }

    myNewFunction2();
    // console.log(nameInside);



// Using alert()
// alert() allows us to show a small  window at the top of our  browswer page to show information to our users.



alert("Hello World!"); // This will be executer immediately


function showAlert(){
    alert("Hello User!");
}

showAlert();

console.log("I will only log in the console when alert is dismissed")


// Prompt
// prompt() allows us to show a small window at the browser t ogather user input. It, much like alert(), will have the page wait until the user complets or enters their input. The input from the prompt() will be returned as a String once the user dismisses the window.


// let samplePrompt = prompt("Enter your name");

// console.log ("Hello " + samplePrompt);
// console.log (typeof samplePrompt);


// prompt() can be used to gather user input
// prompt() can berun immediately


function printWelcomeMessage(){
    let firstName = prompt("Enter you first name: ");
    let lastName = prompt("Enter you last name" );

    console.log("Hello " + firstName + " " + lastName + "!");
    console.log("Welcome to my page!");
}   


printWelcomeMessage();





// Function Naming Convention
// function names should be definitive of the task it will perform. It usually contains a verb.


function getCourse(){
    let courses = ["Science 101", "Math 101", "English 101"];
    console.log(courses);

}

getCourse();



// Avoid generic names to avoid confusion within your code.


function get(){
    let name = "Jamie";
    console.log(name);
}

get();


// Avoid pointless and inappropriate function names.

function foo(){ 

    // getModulus


    console.log(25%5);
}


foo();


// Name your functions in small caps. Follow camelCase when naming variables and functions


// camelCase --> myNameIsJhemmo
// snakeCase --> my_name_is_jhemmo
// kebab-case --> my- namw-is-jhemmo


function displayCarInfo(){
    console.log("Brand: Toyota");
    console.log("Owner: Me")
    console.log("Color: Red")
}

displayCarInfo();


